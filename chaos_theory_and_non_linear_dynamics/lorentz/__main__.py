import numpy as np
from chaos_theory_and_non_linear_dynamics.utils.phase_portrait_animate import PhasePortraitAnimate3D


class Lorentz:
    def __init__(self):
        self.sigma = 10
        self.b = 8 / 3

    def fx(self, r):
        x, y, z = r
        return self.sigma * (y - x)

    @staticmethod
    def gx_d(d):
        def gx(r):
            x, y, z = r
            return d * x - y - x * z
        return gx

    def hx(self, r):
        x, y, z = r
        return x * y - self.b * z

    def phase_diagram(self, initial_conditions, d, t, divisions):
        tspan = np.linspace(0, t, divisions)
        P = PhasePortraitAnimate3D([self.fx, self.gx_d(d), self.hx])
        ax, _, plt, trajectories = P.get_portrait(initial_conditions, tspan)
        for trajectory in trajectories:
            x, y, z = trajectory
            ax.plot(x, y, z, label="x = {}, y = {}, z = {}".format(x[0], y[0], z[0]), linewidth=0.7)
        plt.legend(title='Initial Conditions', loc='upper right', fontsize=3)
        plt.show()

    def time_series_diagram(self, initial_conditions, d, t, divisions, axis):
        tspan = np.linspace(0, t, divisions)
        P = PhasePortraitAnimate3D([self.fx, self.gx_d(d), self.hx])
        plt = P.get_time_series(initial_conditions, tspan, axis)
        plt.legend(title='Initial Conditions', loc='upper right', fontsize=3)
        plt.show()

    def phase_animate(self, initial_conditions, d, t, divisions):
        tspan = np.linspace(0, t, divisions)
        P = PhasePortraitAnimate3D([self.fx, self.gx_d(d), self.hx])
        _, plt = P.get_animation(initial_conditions, tspan)
        plt.show()


L = Lorentz()
ics = [
    # (5, 2, 0),
    # (5, 13, 1),
    # (18, 3, 2),
    # (10, 1, 3),
    # (-5, -5, -5),
    (5, 5, 5),
    # (10, 10, 0),
    (0, 10, 10),
    # (10, 0, 10),
    # (0, 0, 0),
    # (0, 1, 1),
    # (-10, 10, 0),
    # (10, -10, 0),
    # (0, 10, -10)
]
# L.phase_diagram(ics, 20, 1000, 50000)
L.time_series_diagram(ics, 20, 100, 1000, 0)
