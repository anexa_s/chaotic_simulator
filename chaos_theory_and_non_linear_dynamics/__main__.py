import argparse
from chaos_theory_and_non_linear_dynamics.logistic_map.__main__ import LogisticMap


def logistic_commands(logistic_parser):
    group = logistic_parser.add_mutually_exclusive_group()
    group.add_argument(
        "-c", "--cobweb",
        help="to draw cobweb diagram of the logistic map",
        nargs=4,
        action="store"
    )


def lorentz_commands(lorentz_parser):
    group = lorentz_parser.add_mutually_exclusive_group()
    group.add_argument(
        "-pa", "--phaseanimate",
        help="to draw phase portrait animations of the lorentz model",
        nargs=4,
        action="store"
    )


def main():
    print("Chaos Theory and Non Linear Dynamics")
    parser = argparse.ArgumentParser(description='chaos numerical simulator')
    subparsers = parser.add_subparsers(dest='command', help="sub commands")
    logistic_parser = subparsers.add_parser("logistic")
    logistic_commands(logistic_parser)

    args = parser.parse_args()

    if args.command == 'logistic':
        L = LogisticMap()
        if args.cobweb:
            print(args.cobweb)


if __name__ == '__main__':
    main()
