import numpy as np
from chaos_theory_and_non_linear_dynamics.utils.phase_portrait_animate import PhasePortraitAnimate2D


class Q2a:
    def __init__(self):
        for u in np.linspace(-0.2, 0.2, 5):
            def fx(r):
                x, y = r
                return y + u * x

            def gx(r):
                x, y = r
                return -x + u * y - x * x * y

            P = PhasePortraitAnimate2D([fx, gx])
            ics = [
                (0.001, 0.0001),
                (1, 0),
                (0, 1),
                (1, 1),
                (-1, 0),
                (0, -1),
                (-1, -1)
            ]

            tspan = np.linspace(0, 1000, 10000)
            ax, _, plt, trajectories = P.get_portrait(ics, tspan)
            for trajectory in trajectories:
                x, y = trajectory
                ax.plot(x, y, label="x = {}, y = {}".format(x[0], y[0]), linewidth=0.7)
            plt.legend(title='Initial Conditions (u = {:.2f})'.format(u), loc='upper right', fontsize=5)
            plt.savefig("Q2a_u_{:.2f}.png".format(u))
            plt.show()


class Q2b:
    def __init__(self):
        for u in np.linspace(-0.2, 0.2, 5):
            def fx(r):
                x, y = r
                return u * x + y - x * x * x

            def gx(r):
                x, y = r
                return -x + u * y - 2 * y * y * y

            P = PhasePortraitAnimate2D([fx, gx])
            ics = [
                (0.001, 0.0001),
                (1, 0),
                (0, 1),
                (1, 1),
                (-1, 0),
                (0, -1),
                (-1, -1)
            ]

            tspan = np.linspace(0, 1000, 10000)
            ax, _, plt, trajectories = P.get_portrait(ics, tspan)
            for trajectory in trajectories:
                x, y = trajectory
                ax.plot(x, y, label="x = {}, y = {}".format(x[0], y[0]), linewidth=0.7)
            plt.legend(title='Initial Conditions (u = {:.2f})'.format(u), loc='upper right', fontsize=5)
            plt.savefig("Q2b_u_{:.2f}.png".format(u))
            plt.show()


class Q2c:
    def __init__(self):
        for u in np.linspace(-0.02, 0.02, 5):
            def fx(r):
                x, y = r
                return u * x + y - x * x

            def gx(r):
                x, y = r
                return -x + u * y - 2 * x * x

            P = PhasePortraitAnimate2D([fx, gx])
            ics = [
                (0.001, 0.001),
                (0.18, 0),
                (0.17, 0),
                (0, 0.2),
                (0, -0.2),
                (0.2, 0),
                (-0.1, 0.1),
                (-2.5, 2.5)
            ]

            tspan = np.linspace(0, 1000, 10000)
            ax, _, plt, trajectories = P.get_portrait(ics, tspan)
            for trajectory in trajectories:
                x, y = trajectory
                ax.plot(x, y, label="x = {}, y = {}".format(x[0], y[0]), linewidth=0.7)
            ax.set_xlim(-0.5, 0.5)
            ax.set_ylim(-0.5, 0.5)
            plt.legend(title='Initial Conditions (u = {:.2f})'.format(u), loc='upper right', fontsize=5)
            plt.savefig("Q2c_u_{:.2f}.png".format(u))
            plt.show()


class PreyPredator:
    def __init__(self):
        for a in np.linspace(0.4, 0.6, 10):
            def fx(r):
                x, y = r
                return x * (x * (1 - x) - y)

            def gx(r):
                x, y = r
                return y * (x - a)

            P = PhasePortraitAnimate2D([fx, gx])
            ics = [
                (0.2, 0.4),
                (0.4, 0.6),
                (0.4, 0.3),
                (0.4, 0.2),
                (0.5, 0.25),
            ]

            tspan = np.linspace(0, 1000, 10000)
            ax, _, plt, trajectories = P.get_portrait(ics, tspan)
            for trajectory in trajectories:
                x, y = trajectory
                ax.plot(x, y, label="x = {}, y = {}".format(x[0], y[0]), linewidth=0.7)
            plt.legend(title='Initial Conditions (a = {:.2f})'.format(a), loc='upper right', fontsize=5)
            plt.savefig("pp_a_{:.2f}.png".format(a))
            plt.show()


# PreyPredator()
# tspan = np.linspace(0, 20, 500)
#
# alpha = 3
# beta = .1
# gamma = .8
# delta = .03
#
#
# def fx(r):
#     x, y = r
#     return alpha * x - beta * x * y
#
#
# def gx(r):
#     x, y = r
#     return delta * x * y - gamma * y
#
#
# odes = (fx, gx)
# tr = Trajectory(odes)
#
# prey, predator = tr.get_trajectory((50, 20), tspan)
#
# fig, ax = plt.subplots()
# ax.set_xlim(0, tspan[-1])
# ax.set_ylim(0, max(max(predator), max(prey)))
#
# t_data = [tspan[0]]
# prey_data = [prey[0]]
# predator_data = [predator[0]]
#
# prey_line, = ax.plot(tspan[0], prey[0])
# predator_line, = ax.plot(tspan[0], predator[0])
# prey_line.set_label('Prey')
# predator_line.set_label('Predator')
#
#
# def animate(i):
#     t_data.append(tspan[i])
#     prey_data.append(prey[i])
#     predator_data.append(predator[i])
#
#     prey_line.set_xdata(t_data)
#     prey_line.set_ydata(prey_data)
#
#     predator_line.set_xdata(t_data)
#     predator_line.set_ydata(predator_data)
#     return prey_line, predator_line
#
#
# ani = animation.FuncAnimation(
#     fig, func=animate, frames=np.arange(0, len(tspan), 1), interval=25, repeat=False)
#
# plt.legend()
# plt.show()
