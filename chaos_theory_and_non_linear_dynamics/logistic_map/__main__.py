import numpy as np
from chaos_theory_and_non_linear_dynamics.utils.cobweb_animate import CobWebAnimate2D, TimeSeriesAnimate
import matplotlib.pyplot as plot


class LogisticMap:
    def __init__(self):
        pass

    @staticmethod
    def logistic_map_r(r):
        def logistic_map(x):
            return r * np.multiply(x, np.subtract(1, x))
        return logistic_map

    def logistic_map_n(self, n, r):
        def logistic_map(x):
            for i in range(n):
                x = self.logistic_map_r(r)(x)
            return x
        return logistic_map

    def cobweb_diagram(self, r, initial_conditions, steps, plot_range):
        c = CobWebAnimate2D(self.logistic_map_r(r))
        _, plt = c.get_animation(initial_conditions, steps, plot_range)
        plt.suptitle("Logistic Map Cobweb")
        plt.show()

    def cobweb_diagram_n(self, r, initial_conditions, steps, plot_range, n):
        c = CobWebAnimate2D(self.logistic_map_n(n, r))
        _, plt = c.get_animation(initial_conditions, steps, plot_range)
        plt.suptitle("Logistic Map Cobweb")
        plt.show()

    def bifurcation_diagram(self, iterations, limit, divisions):
        r = np.linspace(1.0, 4.0, divisions)
        x = 1e-5 * np.ones(divisions)
        for i in range(iterations):
            x = self.logistic_map_r(r)(x)
            if i >= iterations - limit:
                plot.plot(r, x, ',k', alpha=0.25)
        plot.show()

    def time_series_diagram(self, r, initial_conditions, t):
        ts = TimeSeriesAnimate(self.logistic_map_r(r))
        _, plt = ts.get_animation(initial_conditions, t)
        plt.show()

    def lyoupnov_exponent(self):
        pass


L = LogisticMap()
# L.time_series_diagram(3.8282, [0.3001, 0.3], 100)
# L.cobweb_diagram_n(3.5, [0.2], 10, [0, 1], 10)
# L.cobweb_diagram(3.5, [0.2], 10, [0, 1])
# L.bifurcation_diagram(1000, 100, 10000)

# import matplotlib.pyplot as plt
# import numpy as np
# from moviepy.editor import VideoClip
# from moviepy.video.io.bindings import mplfig_to_npimage
#
# x = np.linspace(-2, 2, 200)
#
# duration = 10
#
# fig, ax = plt.subplots(dpi=100)
# def make_frame(t):
#     r = t / duration * 3.99
#     ax.clear()
#     plot_logistic_iterated_2D(0.3, r, ax=ax)
#     return mplfig_to_npimage(fig)
#
# animation = VideoClip(make_frame, duration=duration)
# plt.close(fig)
# animation.ipython_display(fps=25, loop=True, autoplay=True)