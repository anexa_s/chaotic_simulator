class Cobweb:
    def __init__(self, ode):
        self.__ode = ode

    def get_cobweb(self, initial_condition, t):
        p = [(initial_condition, 0)]
        for i in range(t):
            prev_point = p.__getitem__(-1)
            if i % 2 == 0:
                next_point = (prev_point[0], self.__ode(prev_point[0]))
                p.append(next_point)
            else:
                next_point = (prev_point[1], prev_point[1])
                p.append(next_point)
        x = [coordinate[0] for coordinate in p]
        y = [coordinate[1] for coordinate in p]
        return x, y


class TimeSeries:
    def __init__(self, ode):
        self.__ode = ode

    def get_time_series(self, initial_condition, t):
        p = [initial_condition]
        curr_point = initial_condition
        for i in range(0, t-1):
            curr_point = self.__ode(curr_point)
            p.append(curr_point)
        return p

