""""Vander pol oscillator"""
from scipy import integrate


class Trajectory:
    def __init__(self, odes):
        self.__odes = odes

    def f(self, t, r):
        fx = [ode(r) for ode in self.__odes]
        return fx

    def solve(self, r0, tspan):
        return integrate.solve_ivp(self.f, [tspan[0], tspan[-1]], r0, t_eval=tspan)

    def get_trajectory(self, initial_condition, tspan):
        graph = self.solve(initial_condition, tspan)
        r = graph.y
        return r
