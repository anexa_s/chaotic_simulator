import matplotlib.pyplot as plt
from matplotlib import animation
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from chaos_theory_and_non_linear_dynamics.utils.phase_portrait import Trajectory


class PhasePortraitAnimate2D:
    def __init__(self, odes):
        assert len(odes) == 2
        self.__odes = odes

    def get_portrait(self, initial_conditions, tspan):
        tr = Trajectory(self.__odes)

        fig, ax = plt.subplots()
        fig.suptitle('Phase Portrait 2D')

        trajectories = []
        mn_x = np.inf
        mn_y = np.inf
        mx_x = -np.inf
        mx_y = -np.inf
        for initial_condition in initial_conditions:
            x, y = tr.get_trajectory(initial_condition, tspan)
            mn_x = min(mn_x, min(x))
            mn_y = min(mn_y, min(y))

            mx_x = max(mx_x, max(x))
            mx_y = max(mx_y, max(y))
            trajectories.append((x, y))

        ax.set_xlim(mn_x, mx_x)
        ax.set_ylim(mn_y, mx_y)
        plt.grid(True)
        plt.xlabel("x-axis")
        plt.ylabel("y-axis")
        return ax, fig, plt, trajectories

    def get_animation(self, initial_conditions, tspan):
        ax, fig, plt, trajectories = self.get_portrait(initial_conditions, tspan)

        data = []
        lines = []

        for trajectory in trajectories:
            x, y = trajectory
            data.append(([], []))
            data[-1][0].append(x[0])
            data[-1][1].append(y[0])

            line, = ax.plot(x[0], y[0])
            line.set_label('x = {}, y = {}'.format(x[0], y[0]))
            lines.append(line)

        def animate(i):
            for j in range(0, len(trajectories)):
                x, y = trajectories[j]
                data[j][0].append(x[i])
                data[j][1].append(y[i])

                lines[j].set_xdata(data[j][0])
                lines[j].set_ydata(data[j][1])

            return lines

        ani = animation.FuncAnimation(
            fig, func=animate, frames=np.arange(0, len(tspan), 1), interval=25, repeat=False)

        plt.legend(title='Initial Conditions')
        plt.grid(True)
        plt.xlabel("x-axis")
        plt.ylabel("y-axis")
        return ani, plt


class PhasePortraitAnimate3D:
    def __init__(self, odes):
        assert len(odes) == 3
        self.__odes = odes

    def get_portrait(self, initial_conditions, tspan):
        tr = Trajectory(self.__odes)

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        fig.suptitle('Phase Portrait 3D')

        trajectories = []
        mn_x = np.inf
        mn_y = np.inf
        mn_z = np.inf
        mx_x = -np.inf
        mx_y = -np.inf
        mx_z = -np.inf
        for initial_condition in initial_conditions:
            x, y, z = tr.get_trajectory(initial_condition, tspan)
            mn_x = min(mn_x, min(x))
            mn_y = min(mn_y, min(y))
            mn_z = min(mn_z, min(z))

            mx_x = max(mx_x, max(x))
            mx_y = max(mx_y, max(y))
            mx_z = max(mx_z, max(z))
            trajectories.append((x, y, z))

        ax.set_xlim(mn_x, mx_x)
        ax.set_ylim(mn_y, mx_y)
        ax.set_zlim(mn_z, mx_z)
        plt.grid(True)
        plt.xlabel("x-axis")
        plt.ylabel("y-axis")
        return ax, fig, plt, trajectories

    def get_time_series(self, initial_conditions, tspan, axis):
        tr = Trajectory(self.__odes)

        for i, initial_condition in enumerate(initial_conditions):
            trajectory = tr.get_trajectory(initial_condition, tspan)
            plt.plot(tspan, trajectory[axis],
                     label="x = {}, y = {}, z = {}".format(trajectory[0][0], trajectory[0][1], trajectory[0][2]),
                     linewidth=0.7)
            plt.grid(True)
            plt.xlabel("time")
            if axis == 0:
                plt.ylabel("x-axis")
            elif axis == 1:
                plt.ylabel("y-axis")
            elif axis == 2:
                plt.ylabel("z-axis")
        return plt

    def get_animation(self, initial_conditions, tspan):
        ax, fig, plt, trajectories = self.get_portrait(initial_conditions, tspan)

        data = []
        lines = []

        for trajectory in trajectories:
            x, y, z = trajectory
            data.append(([], [], []))
            data[-1][0].append(x[0])
            data[-1][1].append(y[0])
            data[-1][2].append(z[0])

            line, = ax.plot(x[0], y[0], z[0], linewidth=0.7)
            line.set_label('x = {}, y = {}, z = {}'.format(x[0], y[0], z[0]))
            lines.append(line)

        def animate(i):
            for j in range(0, len(trajectories)):
                x, y, z = trajectories[j]
                data[j][0].append(x[i])
                data[j][1].append(y[i])
                data[j][2].append(z[i])
                lines[j].set_data_3d(data[j])

            return lines

        ani = animation.FuncAnimation(
            fig, func=animate, frames=np.arange(0, len(tspan), 1), interval=1, repeat=False)

        plt.legend(title='Initial Conditions', loc='upper right', fontsize=3)
        plt.grid(True)
        plt.xlabel("x-axis")
        plt.ylabel("y-axis")
        return ani, plt


# TODO: parameters: update time, line width
