import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np
from chaos_theory_and_non_linear_dynamics.utils.cobweb import Cobweb, TimeSeries


class TimeSeriesAnimate:
    def __init__(self, odes):
        self.__odes = odes

    def get_animation(self, initial_conditions, t, interval=25, window=None):
        ts = TimeSeries(self.__odes)
        fig, ax = plt.subplots()
        fig.suptitle('Time Series Data')

        trajectories = []
        mn_x = np.inf
        mx_x = -np.inf
        for initial_condition in initial_conditions:
            x = ts.get_time_series(initial_condition, t)
            mn_x = min(mn_x, min(x))
            mx_x = max(mx_x, max(x))

            trajectories.append((np.arange(t), x))

        ax.set_ylim(mn_x, mx_x)
        ax.set_xlim(0, t)

        data = []
        lines = []

        for trajectory in trajectories:
            x, y = trajectory
            data.append(([], []))
            data[-1][0].append(x[0])
            data[-1][1].append(y[0])

            line, = ax.plot(x[0], y[0])
            line.set_label('x = {}'.format(y[0]))
            lines.append(line)

        def animate(i):
            for j in range(0, len(trajectories)):
                x, y = trajectories[j]
                data[j][0].append(x[i])
                data[j][1].append(y[i])

                lines[j].set_xdata(data[j][0])
                lines[j].set_ydata(data[j][1])
            if window:
                ax.set_xlim(trajectories[0][0][i] - window, trajectories[0][0][i] + window)

            return lines

        ani = animation.FuncAnimation(
            fig, func=animate, frames=np.arange(0, t, 1), interval=interval, repeat=False)

        plt.legend(title='Initial Conditions')
        plt.grid(True)
        plt.xlabel("x-axis")
        plt.ylabel("y-axis")
        return ani, plt


class CobWebAnimate2D:
    def __init__(self, odes):
        self.__odes = odes

    @staticmethod
    def plot_line(mn_x, mx_x, t, ax):
        x = np.linspace(mn_x, mx_x, t * 1000)
        y = x
        ax.plot(x, y)

    def plot_function(self, mn_x, mx_x, t, ax):
        x = np.linspace(mn_x, mx_x, t * 1000)
        y = self.__odes(x)
        ax.plot(x, y)

    def get_animation(self, initial_conditions, t, plot_range):
        cw = Cobweb(self.__odes)
        fig, ax = plt.subplots()
        fig.suptitle('Cobweb 2D')

        trajectories = []
        mn_x = np.inf
        mn_y = np.inf
        mx_x = -np.inf
        mx_y = -np.inf
        for initial_condition in initial_conditions:
            x, y = cw.get_cobweb(initial_condition, t)
            mn_x = min(mn_x, min(x))
            mn_y = min(mn_y, min(y))

            mx_x = max(mx_x, max(x))
            mx_y = max(mx_y, max(y))
            trajectories.append((x, y))

        mn_x = min(mn_x, min(plot_range))
        mx_x = max(mx_x, max(plot_range))
        mn_y = min(mn_y, min(self.__odes(plot_range)), mn_x)
        mx_y = max(mx_y, max(self.__odes(plot_range)), mx_x)

        ax.set_xlim(mn_x, mx_x)
        ax.set_ylim(mn_y, mx_y)

        self.plot_line(mn_x, mx_x, t, ax)
        self.plot_function(mn_x, mx_x, t, ax)

        data = []
        lines = []

        for trajectory in trajectories:
            x, y = trajectory
            data.append(([], []))
            data[-1][0].append(x[0])
            data[-1][1].append(y[0])

            line, = ax.plot(x[0], y[0])
            line.set_label('x = {}, y = {}'.format(x[0], y[0]))
            lines.append(line)

        def animate(i):
            for j in range(0, len(trajectories)):
                x, y = trajectories[j]
                data[j][0].append(x[i])
                data[j][1].append(y[i])

                lines[j].set_xdata(data[j][0])
                lines[j].set_ydata(data[j][1])

            return lines

        ani = animation.FuncAnimation(
            fig, func=animate, frames=np.arange(0, t, 1), interval=500, repeat=False)

        plt.legend(title='Initial Conditions')
        plt.grid(True)
        plt.xlabel("x-axis")
        plt.ylabel("y-axis")
        return ani, plt
