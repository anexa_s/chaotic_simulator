import matplotlib.pyplot as plt
import numpy as np


class VectorField:
    def __init__(self):
        pass

    @staticmethod
    def get_vector_field_2d(gran, bound, fx, gx):
        x_val = np.arange(-bound, bound, gran)
        y_val = np.arange(-bound, bound, gran)
        x, y = np.meshgrid(x_val, y_val)
        f = fx(x, y)
        g = gx(x, y)
        mp = np.sqrt(np.square(f) + np.square(g))
        fig, ax = plt.subplots()
        Q = ax.quiver(x, y, f, g, mp, cmap='coolwarm')
        fig.colorbar(Q, extend='max')
        return plt


def fx(x, y):
    return np.square(x)


def gx(x, y):
    return np.square(y)


v = VectorField()
v.get_vector_field_2d(1, 10, fx, gx).show()
