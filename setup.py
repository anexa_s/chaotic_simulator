from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='chaos',
    version='0.0.1',
    description='Chaos Theory',
    long_description=readme(),
    long_description_content_type='text/markdown',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent'
    ],
    author='ktk54x',
    author_email='kartikeya.0005@gmail.com',
    keywords='Chaos Theory',
    packages=['chaos_theory_and_non_linear_dynamics'],
    install_requires=[
        'matplotlib==3.4.3',
        'dash==2.0.0',
        'plotly==5.3.1',
        'numpy==1.21.2',
        'scipy==1.7.1',
    ],
    include_package_data=True,
    entry_points={
            'console_scripts': [
                'chaos = chaos_theory_and_non_linear_dynamics.__main__:main'
            ]
    }
)
